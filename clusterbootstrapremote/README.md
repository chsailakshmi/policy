# clusterbootstrap

## Description

Packages for bootstraping amcop cluster. Below is the list of packages:

- config maagement operator
- configsync CRD
- rootSync CR
- workloadCluster CR
- porch repository
- nephio token for porch
- nephio token for configsync
